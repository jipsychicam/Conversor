package jipsychica.facci.convertidordetemperatura;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    TextView Centi, Fahren;
    EditText NumeroCenti, NumeroFahren;
    Button ConvertirCenti, ConvertirFahren;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        NumeroCenti = (EditText)findViewById(R.id.txtC);
        ConvertirFahren = (Button)findViewById(R.id.btnF);
        Fahren = (TextView)findViewById(R.id.lblF);

        NumeroFahren = (EditText)findViewById(R.id.txtF);
        ConvertirCenti = (Button)findViewById(R.id.btnC);
        Centi = (TextView)findViewById(R.id.lblC);

        ConvertirCenti.setOnClickListener(this);
        ConvertirFahren.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnF:
                if (NumeroFahren.getText().toString().isEmpty()){
                }else {
                    Double Fahren = Double.valueOf(NumeroFahren.getText().toString());
                    Double Centig = (Fahren - 32) / 1.8;
                    Centi.setText(String.valueOf(Centig)+ " ºC");
                }
                break;

            case R.id.btnC:
                if (NumeroCenti.getText().toString().isEmpty()){
                }else{
                    Double Centig = Double.valueOf(NumeroCenti.getText().toString());
                    Double Fahrenh = (Centig *1.8 ) + 32;
                    Fahren.setText(String.valueOf(Fahrenh)+ " ºF");
                }
                break;

        }
    }
}
